const express = require('express')
const app = express();

const PORT =process.env.PORT || 5000

app.use('/', require('./ipl'))

app.listen(PORT, ()=>console.log("server started at PORT 5000"))