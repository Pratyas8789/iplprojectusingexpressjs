const express = require('express')
const router = express.Router()
const csvtojson = require('csvtojson')
const fs = require('fs')
const path = require('path')

router.get('/', (req, res) => {
    res.sendFile(path.resolve("src/public/index.html"))
})

router.get('/app.js', (req, res) => {
    res.sendFile(path.resolve("src/public/app.js"))
})

router.get('/style.css', (req, res) => {
    res.sendFile(path.resolve("src/public/style.css"))
})

router.get('/matchesPerYear', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchResults) => {
            try {
                const matchesPerYear = {};
                if (Array.isArray(matchResults)) {
                    matchResults.map((playmatch) => {
                        if (typeof (matchesPerYear[playmatch.season]) === 'undefined') {
                            matchesPerYear[playmatch.season] = 0;
                        }
                        matchesPerYear[playmatch.season]++;
                    })
                    res.send(matchesPerYear)
                    fs.writeFileSync("src/public/output/matchesPerYear.json", JSON.stringify(matchesPerYear, null, 2));
                }
            }
            catch (err) {
                console.error(err);
            }
        })

})


router.get('/wonTeamPerYear', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchesResults) => {
            try {
                if (Array.isArray(matchesResults)) {
                    const wonTeamPerYear = {};
                    matchesResults.map((data) => {
                        const season = data["season"]
                        const winner = data["winner"]
                        if (wonTeamPerYear[season]) {
                            const obj = wonTeamPerYear[season]
                            if (obj[winner]) {
                                obj[winner]++;
                            } else {
                                obj[winner] = 1
                            }
                        }
                        else {
                            let teamWon = {}
                            teamWon[winner] = 1
                            wonTeamPerYear[season] = teamWon;
                        }

                    })
                    res.send(wonTeamPerYear)
                    fs.writeFileSync("src/public/output/wonTeamPerYear.json", JSON.stringify(wonTeamPerYear, null, 2));
                }
            }
            catch (err) {
                console.error(err);
            }
        })
})

router.get('/extraRunsPerTeamYear2016', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchesResults) => {
            const Id = []
            try {
                if (Array.isArray(matchesResults)) {
                    matchesResults.map((data) => {
                        if (data['season'] == '2016') {
                            Id.push(data['id'])
                        }
                    })
                }
            }
            catch (err) {
                console.error(err);
            }
            const extraRunsPerTeam = {}
            csvtojson()
                .fromFile('src/data/deliveries.csv')
                .then((deliveriesResults) => {
                    try {
                        if (Array.isArray(deliveriesResults)) {
                            Id.map((eachId) => {
                                deliveriesResults.map((data) => {
                                    if (data['extra_runs'] > 0) {
                                        if (eachId == data['match_id']) {
                                            if (extraRunsPerTeam[data['bowling_team']]) {
                                                (extraRunsPerTeam[data['bowling_team']]) += (+data['extra_runs'])
                                            } else {
                                                extraRunsPerTeam[data['bowling_team']] = (+data['extra_runs'])
                                            }
                                        }
                                    }
                                })
                            })
                            res.send(extraRunsPerTeam)
                            fs.writeFileSync("src/public/output/extraRunsPerTeamYear2016.json", JSON.stringify(extraRunsPerTeam, null, 2));
                        }
                    }
                    catch (err) {
                        console.error(err);
                    }
                })
        })
})
router.get('/economicalBolwerYear2015', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchesResults) => {
            const Id = []
            try {
                if (Array.isArray(matchesResults)) {
                    matchesResults.map((data) => {
                        if (data['season'] == '2015') {
                            Id.push(data['id'])
                        }
                    })
                }
            }
            catch (err) {
                console.error(err);
            }
            csvtojson()
                .fromFile('src/data/deliveries.csv')
                .then((deliveriesResults) => {
                    try {
                        const bowlers = []; //all bowlers presents
                        if (Array.isArray(deliveriesResults)) {
                            Id.map((eachId) => {
                                deliveriesResults.map((data) => {
                                    if (data['match_id'] === eachId) {
                                        if (!bowlers.includes(data['bowler'])) bowlers.push(data['bowler']);
                                    }
                                })
                            })
                        }

                        const ballerAndItsEconomicBowlers = {}
                        bowlers.map((bowlerName) => {
                            let totalRuns = 0;
                            let totalOvers = 0;
                            deliveriesResults.map((data) => {
                                if (data['bowler'] === bowlerName) {
                                    totalRuns += parseInt(data['total_runs'])
                                    if (data['ball'] === '6') totalOvers++;
                                }
                            })
                            const economicRate = +(totalRuns / totalOvers).toFixed(2)
                            ballerAndItsEconomicBowlers[bowlerName] = economicRate;
                        })

                        let sortable = [];
                        for (let key in ballerAndItsEconomicBowlers) {
                            sortable.push([key, ballerAndItsEconomicBowlers[key]]);
                        }
                        sortable.sort(function (a, b) {
                            return b[1] - a[1];
                        })
                        const finalobj = {}
                        for (let a = 0; a < 10; a++) {
                            finalobj[sortable[a][0]] = sortable[a][1];
                        }
                        res.send(finalobj)
                        fs.writeFileSync("src/public/output/economicalBolwerYear2015.json", JSON.stringify(finalobj, null, 2));

                    }
                    catch (err) {
                        console.error(err);
                    }
                })
        })
})



router.get('/eachTeamWonTossAndMatch', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchesData) => {
            try {
                const tossAndMatchWinner = {};
                if (Array.isArray(matchesData)) {
                    matchesData.map((data) => {
                        if (data['toss_winner'] == data['winner']) {
                            if (tossAndMatchWinner[data['toss_winner']]) {
                                tossAndMatchWinner[data['toss_winner']]++
                            } else {
                                tossAndMatchWinner[data['toss_winner']] = 1
                            }
                        }
                    })
                    res.send(tossAndMatchWinner)
                    fs.writeFileSync("src/public/output/eachTeamWonTossAndMatch.json", JSON.stringify(tossAndMatchWinner, null, 2));
                }
            }
            catch (err) {
                console.error(err);
            }
        })
})


router.get('/playerOfTheMatchEachYear', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchesData) => {
            try {
                if (Array.isArray(matchesData)) {
                    const yearWisePlayerOfMatchEachSeason = {};
                    matchesData.map((data) => {
                        const season = data['season'];
                        const playerOfMatch = data['player_of_match']
                        if (yearWisePlayerOfMatchEachSeason[season]) {
                            let obj = yearWisePlayerOfMatchEachSeason[season];
                            if (obj[playerOfMatch]) {
                                obj[playerOfMatch]++;
                            } else {
                                obj[playerOfMatch] = 1
                            }
                        }
                        else {
                            let teamWon = {}
                            teamWon[playerOfMatch] = 1
                            yearWisePlayerOfMatchEachSeason[season] = teamWon;
                        }
                    })
                    const yearWiseHighestPlayerOfTheMatch = {}
                    for (let key1 in yearWisePlayerOfMatchEachSeason) {
                        const obj = yearWisePlayerOfMatchEachSeason[key1];
                        let max = 0;
                        for (let key2 in obj) {
                            const times = obj[key2]
                            if (max <= times) {
                                max = times;
                            }
                        }
                        const newobj = {}
                        for (let key3 in obj) {
                            if (obj[key3] == max) {
                                newobj[key3] = max;
                            }
                        }
                        yearWiseHighestPlayerOfTheMatch[key1] = newobj

                    }
                    res.send(yearWiseHighestPlayerOfTheMatch)
                    fs.writeFileSync("src/public/output/playerOfTheMatchEachYear.json", JSON.stringify(yearWiseHighestPlayerOfTheMatch, null, 2));
                }
            }
            catch (err) {
                console.error(err);
            }
        })
})

router.get('/strikeRateEachSeason', (req, res) => {
    csvtojson()
        .fromFile('src/data/matches.csv')
        .then((matchesResults) => {
            const Id = {}
            try {
                if (Array.isArray(matchesResults)) {
                    matchesResults.map((data) => {
                        Id[data['id']] = data['season']
                    })
                }
            }
            catch (err) {
                console.error(err);
            }
            csvtojson()
                .fromFile('src/data/deliveries.csv')
                .then((deliveriesResults) => {
                    try {
                        const yearBatsmanarray = {}
                        deliveriesResults.map((data) => {
                            const deliveryID = data['match_id'];
                            if (Id.hasOwnProperty(deliveryID)) {
                                const year = Id[deliveryID];
                                const batsman = data['batsman'];
                                const runs = parseInt(data['batsman_runs'])
                                if (!yearBatsmanarray.hasOwnProperty(year)) {
                                    let temp = {}
                                    let tempA = new Array(2)
                                    tempA[0] = parseInt(runs)
                                    tempA[1] = 1
                                    temp[batsman] = tempA;
                                    yearBatsmanarray[year] = temp
                                }
                                else {
                                    let temp = yearBatsmanarray[year]
                                    let tempA = new Array(2)
                                    if (!temp.hasOwnProperty(batsman)) {
                                        tempA[0] = runs;
                                        tempA[1] = parseInt(1);

                                        temp[batsman] = tempA;
                                    }
                                    else {
                                        const r = temp[batsman][0] + runs;
                                        const b = temp[batsman][1] + 1;
                                        tempA[0] = r;
                                        tempA[1] = b;

                                        temp[batsman] = tempA;
                                    }
                                    yearBatsmanarray[year] = temp;
                                }

                            }
                        })
                        const strikeRatePerPlayer = {}
                        for (key in yearBatsmanarray) {
                            let yearBatsmanarrayOfValue = yearBatsmanarray[key]
                            const nameAndStrike = {}
                            for (let key2 in yearBatsmanarrayOfValue) {
                                let strike = (yearBatsmanarrayOfValue[key2][0] * 100 / yearBatsmanarrayOfValue[key2][1])
                                nameAndStrike[key2] = strike.toFixed(2)
                            }
                            strikeRatePerPlayer[key] = nameAndStrike
                        }
                        res.send(strikeRatePerPlayer)
                        fs.writeFileSync("src/public/output/strikeRateEachSeason.json", JSON.stringify(strikeRatePerPlayer, null, 2));
                    }
                    catch (err) {
                        console.error(err);
                    }
                })
        })
})

router.get('/highestTimesDismissedByAnotherPlayer', (req, res) => {
    csvtojson()
        .fromFile('src/data/deliveries.csv')
        .then((deliveriesData) => {
            try {
                const dismisByAnotherPlayer = {}
                if (Array.isArray(deliveriesData)) {
                    deliveriesData.map((data) => {
                        if (data['player_dismissed'] !== '') {
                            if (dismisByAnotherPlayer.hasOwnProperty(data['player_dismissed'])) {
                                const valueobj = dismisByAnotherPlayer[data['player_dismissed']]
                                if (valueobj.hasOwnProperty(data['bowler'])) {
                                    valueobj[data['bowler']]++
                                } else {
                                    valueobj[data['bowler']] = 1
                                }

                            } else {
                                dismisByAnotherPlayer[data['player_dismissed']] = {}
                                dismisByAnotherPlayer[data['player_dismissed']][data['bowler']] = 1
                            }
                        }
                    })
                    res.send(dismisByAnotherPlayer)
                    fs.writeFileSync("src/public/output/highestTimesDismissedByAnotherPlayer.json", JSON.stringify(dismisByAnotherPlayer, null, 2));
                }
            }
            catch (err) {
                console.error(err);
            }
        })
})


router.get('/bestEconomyInSuperOvers', (req, res) => {
    csvtojson()
        .fromFile('src/data/deliveries.csv')
        .then((deliveriesData) => {
            try {
                if (Array.isArray(deliveriesData)) {
                    const bowlerWithEconomy = {}
                    deliveriesData.map((data) => {
                        if (data['is_super_over'] !== '0') {
                            if (typeof (bowlerWithEconomy[data['bowler']]) === 'undefined') {
                                const arr = []
                                arr[0] = parseInt(data['total_runs'])
                                arr[1] = 1
                                bowlerWithEconomy[data['bowler']] = arr;
                            } else {
                                bowlerWithEconomy[data['bowler']][0] += parseInt(data['total_runs'])
                                bowlerWithEconomy[data['bowler']][1]++
                            }
                        }
                    })

                    let max = 0;
                    let name = ""
                    for (let key in bowlerWithEconomy) {
                        let economy = +(bowlerWithEconomy[key][0] / (bowlerWithEconomy[key][1] / 6)).toFixed(2);
                        bowlerWithEconomy[key] = economy
                        if (max < economy) {
                            max = economy
                            name = key
                        }
                    }
                    const bestbowlerWithEconomy = {}
                    bestbowlerWithEconomy[name] = max
                    res.send(bestbowlerWithEconomy)
                    fs.writeFileSync("src/public/output/bestEconomyInSuperOvers.json", JSON.stringify(bestbowlerWithEconomy, null, 2));
                }
            }
            catch (err) {
                console.error(err);
            }
        })
})

router.get('/*', (req, res) => {
    res.send("Please enter correct url")
})

module.exports = router