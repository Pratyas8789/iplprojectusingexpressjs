
function highChart(data, XAxisName, YAxisName, fileName) {
    const keyArray = Object.keys(data);
    const valueArray = Object.values(data);
    if (typeof (valueArray[0]) === "object") {
        const AxisYName = []
        const AxisZName = []
        let i = 0;
        for (let key in valueArray) {
            AxisYName.push(key)
            const obj = {}
            obj["name"] = keyArray[i]
            obj["data"] = Object.entries(valueArray[key])
            obj["data"].map((data) => {
                data[1] = +data[1]
            })
            AxisZName.push(obj)
            i++
        }
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: fileName
            },
            xAxis: {
                categories: AxisYName,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: XAxisName
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: AxisZName,
        });
    } else {
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: fileName
            },
            xAxis: {
                categories: keyArray,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: XAxisName
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: YAxisName,
                data: valueArray

            }]
        });
    }
}

const container = document.getElementById("container")
const fetchData = (fileName, XAxisName, YAxisName) => {
    container.innerHTML = ""
    container.innerHTML =
        `
    <div class=" d-flex align-items-center" >    
        <h1>Please Wait </h1>
        <div class="spinner-border" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </div>    
    `
    fetch(`https://ipl-project-using-express-6bj4.onrender.com/${fileName}`)
        .then((data) => {
            return data.json()
        }).then((data) => {
            container.innerHTML = ""
            highChart(data, XAxisName, YAxisName, fileName)
        })
        .catch((err) => {
            container.innerHTML = `<h1>Error:- 404 not found</h1>`
            console.error(err);
        })
}